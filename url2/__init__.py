from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from views import setRedirect


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application. """
    config = Configurator()
    config.add_route('root', '')
    config.add_route('redirectUrI', '/*fizzle')
    config.add_view(setRedirect, route_name='root')
    config.add_view(setRedirect, route_name='redirectUrI')
    return config.make_wsgi_app()

if __name__ == '__main__':
    config = Configurator()
    config.add_route('root', '')
    config.add_route('redirectUrI', '/fizzle')
    config.add_view(setRedirect, route_name='root')
    config.add_view(setRedirect, route_name='redirectUrI')
    app = config.make_wsgi_app()
    server = make_server('0.0.0.0', 8080, app)
    server.serve_forever()

