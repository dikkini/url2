# coding=utf-8

__author__ = 'dikkini@gmail.com'

from pyramid.httpexceptions import HTTPMovedPermanently, HTTPNotFound
import ConfigParser
from ConfigParser import NoOptionError, NoSectionError


def setRedirect(request):
    config = ConfigParser.ConfigParser()
    config.read('config.ini')
    domain = request.host
    try:
        redirectUrl = config.get(domain, 'redirect')
        isTail = config.get(domain, 'tail')
    except (NoSectionError, NoOptionError) as e:
        return HTTPNotFound()

    if isTail == 'True':
        tail = request.path
        return HTTPMovedPermanently(redirectUrl + tail)
    else:
        return HTTPMovedPermanently(redirectUrl)






